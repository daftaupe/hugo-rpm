%define debug_package %{nil}
%define repo github.com/gohugoio/hugo
Name:           hugo
Version:        0.139.2
Release:        1%{?dist}
Summary:        A Fast and Flexible Static Site Generator

Group:          Applications/System
License:        Apache 2.0
URL:            https://%{repo}
Source0:        https://%{repo}/archive/v%{version}.tar.gz

BuildRequires:  git golang gcc-c++

%description
Hugo is a static HTML and CSS website generator written in Go. It is optimized for speed, easy use and configurability. Hugo takes a directory with content and templates and renders them into a full HTML website.

%prep
mkdir -p %{_builddir}/src/github.com/gohugoio/
cd %{_builddir}/src/github.com/gohugoio/
tar -xvzf %{_sourcedir}/v%{version}.tar.gz 
mv hugo-%{version} hugo
cd hugo

%build
export GOPATH="%{_builddir}"
export PATH=$PATH:"%{_builddir}"/bin
cd %{_builddir}/src/github.com/gohugoio/hugo
export GO111MODULE=on 
go install github.com/magefile/mage@latest
HUGO_BUILD_TAGS=extended $GOPATH/bin/mage hugo
HUGO_BUILD_TAGS=extended $GOPATH/bin/mage install

%install
mkdir -p %{buildroot}%{_bindir}

cp %{_builddir}/bin/hugo %{buildroot}%{_bindir}


%files
%{_bindir}/hugo

%changelog
* Mon Nov 25 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.139.2-1
- New release 0.139.2

* Sun Jun 16 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.127.0-1
- New release 0.127.0

* Sat Mar 9 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.123.8-1
- New release 0.123.8

* Tue Aug 8 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.117.0-1
- New release 0.117.0

* Tue Aug 1 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.116.1-1
- New release 0.116.1

* Fri Jul 14 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.115.3-1
- New release 0.115.3

* Thu Jul 13 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.115.2-1
- New release 0.115.2

* Sun Jul 1 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.115.0-1
- New release 0.115.0

* Mon Jun 5 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.113.0-1
- New release 0.113.0

* Sun Jun 4 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.112.7-1
- New release 0.112.7

* Sun May 28 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.112.4-1
- New release 0.112.4

* Fri May 26 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.112.3-1
- New release 0.112.3

* Sat Mar 18 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.111.3-1
- New release 0.110.3

* Fri Mar 10 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.111.2-1
- New release 0.110.2

* Tue Jan 31 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.110.0-1
- New release 0.110.0

* Fri Dec 23 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.109.0-1
- New release 0.109.0

* Thu Dec 8 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.108.0-1
- New release 0.108.0

* Tue Nov 29 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.107.0-1
- New release 0.107.0

* Sat Oct 29 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.105.0-1
- New release 0.105.0

* Tue Oct 4 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.104.3-1
- New release 0.104.3

* Sat Oct 1 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.104.2-1
- New release 0.104.2
- Switch to build extended version

* Mon Sep 26 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.104.1-1
- New release 0.104.1

* Sat Sep 24 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.104.0-1
- New release 0.104.0

* Sat Sep 17 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.103.0-1
- New release 0.103.0

* Sun Sep 11 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.102.3-1
- New release 0.102.3

* Tue May 3 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.98.0-1
- New release 0.98.0

* Sun Mar 6 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.93.2-1
- New release 0.93.2

* Sun Feb 13 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.92.2-1
- New release 0.92.2

* Tue Feb 1 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.92.1-1
- New release 0.92.1

* Wed Dec 22 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.91.1-1
- New release 0.91.1

* Mon Jul 26 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.86.0-1
- New release 0.86.0

* Mon May 03 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.83.1-1
- New release 0.83.1

* Sat May 01 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.83.0-1
- New release 0.83.0

* Sat Mar 20 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.81.0-1
- New release 0.81.0

* Wed Jan 27 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.80.0-1
- New release 0.80.0

* Thu Nov 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.78.2-1
- New release 0.78.2

* Sat Sep 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.75.1-1
- New release 0.75.1

* Mon Aug 10 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.74.3-1
- New release 0.74.3

* Tue Jul 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.73.0-1
- New release 0.73.0

* Tue Jul 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.72.0-1
- New release 0.72.0

* Tue Jul 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.71.1-1
- New release 0.71.1

* Tue Jul 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.71.0-1
- New release 0.71.0

* Thu May 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.70.0-1
- New release 0.70.0

* Tue Apr 28 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.69.2-1
- New release 0.69.2

* Wed Apr 15 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.69.0-1
- New release 0.69.0

* Mon Mar 23 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.68.1-1
- New release 0.68.1

* Sun Mar 22 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.68.0-1
- New release 0.68.0

* Mon Mar 16 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.67.1-1
- New release 0.67.1

* Tue Mar 10 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.67.0-1
- New release 0.67.0

* Wed Mar 04 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.66.0-1
- New release 0.66.0

* Mon Mar 02 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.65.3-1
- New release 0.65.3

* Tue Feb 11 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.64.1-1
- New release 0.64.1

* Tue Feb 04 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.64.0-1
- New release 0.64.0

* Tue Jan 28 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.63.2-1
- New release 0.63.2

* Fri Jan 24 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.63.1-1
- New release 0.63.1

* Tue Jan 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.62.2-1
- New release 0.62.2

* Fri Sep 20 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.58.3-1
- New release 0.58.3

* Fri Sep 13 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.58.2-1
- New release 0.58.2

* Fri Sep 06 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.58.1-1
- New release 0.58.1

* Thu Sep 05 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.58.0-1
- New release 0.58.0

* Sun Aug 18 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.57.2-1
- New release 0.57.2

* Fri Aug 16 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.57.1-1
- New release 0.57.1

* Wed Aug 14 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.57.0-1
- New release 0.57.0

* Mon Aug 12 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.56.3-1
- New release 0.56.3

* Tue Jul 30 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.56.2-1
- New release 0.56.2

* Mon Jul 29 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.56.1-1
- New release 0.56.1

* Fri Jul 26 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.56.0-1
- New release 0.56.0

* Mon May 20 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.6-1
- New release 0.55.6

* Thu May 02 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.5-1
- New release 0.55.5

* Mon Apr 29 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.4-1
- New release 0.55.4

* Mon Apr 22 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.3-1
- New release 0.55.3

* Thu Apr 18 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.2-1
- New release 0.55.2

* Mon Apr 15 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.1-1
- New release 0.55.1

* Wed Apr 10 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.55.0-1
- New release 0.55.0

* Thu Feb 07 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.54-1
- New release 0.54

* Fri Dec 28 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.53-1
- New release 0.53

* Fri Nov 30 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.52-1
- New release 0.52

* Wed Nov 07 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.51-1
- New release 0.51

* Tue Nov 06 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.50-1
- New release 0.50

* Fri Oct 12 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.49.2-1
- New release 0.49.2

* Thu Oct 11 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.49.1-1
- New release 0.49.1
- Hugo doesn't use dep anymore

* Tue Sep 25 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.49-1
- New release 0.49

* Wed Aug 29 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.48-1
- New release 0.48

* Mon Aug 20 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.47.1-1
- New release 0.47.1

* Mon Aug 20 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.47-1
- New release 0.47

* Wed Jul 25 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.45.1-1
- New release 0.45.1

* Mon Jul 23 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.45-1
- New release 0.45

* Fri Jul 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.44-1
- New release 0.44

* Wed Jul 11 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.43-1
- New release 0.43

* Thu Jun 28 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.42.2-1
- New release 0.42.2

* Wed Jun 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.42.1-1
- New release 0.42.1

* Tue Jun 12 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.42-1
- New release 0.42

* Sat May 26 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.41-1
- New release 0.41

* Wed May 09 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.40.3-1
- New release 0.40.3

* Mon Apr 30 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.40.2-1
- New release 0.40.2

* Thu Apr 26 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.40.1-1
- New release 0.40.1

* Mon Apr 23 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.40-1
- New release 0.40

* Tue Apr 17 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.39-0
- New release 0.39

* Mon Apr 09 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.38.2-0
- New release 0.38.2

* Fri Apr 06 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.38.1-0
- New release 0.38.1

* Wed Apr 04 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.38-0
- New release 0.38

* Thu Mar 15 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.37.1-0
- New release 0.37.1

* Tue Feb 27 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.37-0
- New release 0.37

* Fri Feb 23 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.36.1-0
- New release 0.36.1

* Wed Feb 07 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.36-0
- New release 0.36

* Sat Feb 03 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.35-0
- New release 0.35

* Sun Jan 28 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.34-0
- New release 0.34

* Sun Jan 21 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.33-0
- New release 0.33

* Sat Jan 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.32.4-0
- New release 0.32.4

* Sat Jan 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.32.3-0
- New release 0.32.3

* Sat Jan 06 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.32.2-0
- New release 0.32.2

* Tue Jan 02 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.32.1-0
- New release 0.32.1

* Tue Jan 02 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.32-0
- New release 0.32

* Mon Nov 27 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.31.1-0
- New release 0.31.1

* Mon Nov 20 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.31-0
- New release 0.31

* Thu Oct 19 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.30.2-0
- New release 0.30.2
- Adapt the build process to dep

* Thu Oct 19 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.30.1-0
- New release 0.30.1
- Adapt to mage build process

* Mon Oct 16 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.30-0
- New release 0.30

* Thu Sep 28 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.29-0
- New release 0.29
- Clean the changelog

* Thu Sep 28 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.28-0
- New release 0.28

* Tue Sep 12 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.27-0
- New release 0.27

* Fri Aug 25 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.26-0
- New release 0.26

* Tue Jul 25 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.25.1-0
- New release 0.25.1

* Tue Jul 25 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.25-0
- New release 0.25

* Sat Jun 24 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.24.1-0
- New release 0.24.1

* Thu Jun 22 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.24-0
- New release 0.24

* Fri Jun 16 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.23-0
- New release 0.23
- Update to the new URL of the project on github

* Tue Jun 13 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.22.1-0
- New release 0.22.1
- New release 0.22

* Thu May 25 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.21-0
- New release 0.21

* Sun May 14 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.7-0
- New release 0.20.7

* Sun May 14 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.6-0
- New release 0.20.6

* Sun May 14 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.5-0
- New release 0.20.5

* Sun May 14 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.4-0
- New release 0.20.4

* Sun May 14 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.3-0
- New release 0.20.3

* Tue Apr 18 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.2-0
- New release 0.20.2

* Tue Apr 18 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20.1-0
- New release 0.20.1

* Tue Apr 18 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.20-0
- New release 0.20

* Mon Feb 27 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.19-0
- New release 0.19

* Sat Feb 11 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.18.1-0
- Initial version of the rpm
